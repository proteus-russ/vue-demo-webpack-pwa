import Vue from 'vue'
import Router from 'vue-router'
import TodoListing from '@/components/TodoListing'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Todos',
      component: TodoListing
    }
  ]
})
