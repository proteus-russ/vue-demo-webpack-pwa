import idb from 'idb'

export const TodoStoreName = 'todos'

export default idb.open('todo-app', 1, upgradeDB => {
    upgradeDB.createObjectStore(TodoStoreName, { keyPath: 'id' })
})
