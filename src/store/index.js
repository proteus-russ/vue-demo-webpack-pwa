import { TodoStoreName } from './idb_setup'

import Vue from 'vue'
import Vuex from 'vuex'

import TodoModule from './todo'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        [TodoStoreName]: TodoModule
    }
})
