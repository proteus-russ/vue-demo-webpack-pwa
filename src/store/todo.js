import ulid from 'ulid'
import db, { TodoStoreName } from './idb_setup'

const TodoBaseMutations = {
    setList: 'setList',
    add: 'add',
    delete: 'delete'
}

const TodoBaseActions = {
    add: TodoBaseMutations.add,
    delete: TodoBaseMutations.delete,
    getDBState: 'getDBState'
}

export const TodoMutations = {
    setList: `${TodoStoreName}/${TodoBaseMutations.setList}`,
    add: `${TodoStoreName}/${TodoBaseMutations.add}`,
    delete: `${TodoStoreName}/${TodoBaseMutations.delete}`
}

export const TodoActions = {
    add: `${TodoStoreName}/${TodoBaseActions.add}`,
    delete: `${TodoStoreName}/${TodoBaseActions.delete}`,
    getDBState: `${TodoStoreName}/${TodoBaseActions.getDBState}`
}

export class Todo {
    constructor (message, id) {
        if (!arguments.length) {
            this._id = ulid()
            this._message = ''
        } else if (arguments.length === 1) {
            this._id = ulid()
            this._message = message
        } else {
            this._id = id
            this._message = message
        }
    }

    get id () {
        return this._id
    }

    set id (id) {
        this._id = id
    }

    get message () {
        return this._message
    }

    set message (message) {
        this._message = message
    }

    toJSON () {
        return {
            id: this.id,
            message: this.message
        }
    }

    static fromJSON (obj) {
        return new Todo(obj.message, obj.id)
    }
}

export default {
    namespaced: true,
    state: {
        todos: []
    },
    // Synchronous
    mutations: {
        [TodoBaseMutations.setList] (state, list) {
            state.todos = list
        },

        [TodoBaseMutations.add] (state, todo) {
            state.todos.push(todo)
        },

        [TodoBaseMutations.delete] (state, todo) {
            state.todos = state.todos.filter((t) => { return t.id !== todo.id })
        }
    },
    // Asynchronous
    actions: {
        async [TodoBaseActions.getDBState] (context, todo) {
            let d = await db
            let ts = (await d.transaction(TodoStoreName).objectStore(TodoStoreName).getAll()).map(Todo.fromJSON)
            context.commit(TodoBaseMutations.setList, ts)
        },

        async [TodoBaseActions.add] (context, todo) {
            let d = await db
            let tx = d.transaction(TodoStoreName, 'readwrite')
            tx.objectStore(TodoStoreName).put(todo.toJSON())
            await tx.complete
            context.commit(TodoBaseMutations.add, todo)
        },

        async [TodoBaseActions.delete] (context, todo) {
            let d = await db
            let tx = d.transaction(TodoStoreName, 'readwrite')
            tx.objectStore(TodoStoreName).delete(todo.id)
            await tx.complete
            context.commit(TodoBaseMutations.delete, todo)
        }
    }
}
