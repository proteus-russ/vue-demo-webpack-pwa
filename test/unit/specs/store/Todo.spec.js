import { Todo, TodoMutations, TodoActions } from '@/store/todo'
import db, { TodoStoreName } from '@/store/idb_setup'
import Store from '@/store'

describe('todo Store', async () => {
    describe('mutations', async () => {
        it('should successfully set the list of todos', async () => {
            let todos = [ new Todo('test1'), new Todo('test2') ]
            Store.commit(TodoMutations.setList, todos)
            expect(Store.state[TodoStoreName].todos.length).to.equal(todos.length)
        })

        it('should successfully add a todo', async () => {
            let todo = new Todo('test')
            Store.commit(TodoMutations.add, todo)
            expect(Store.state[TodoStoreName].todos.filter((t) => { return t.id === todo.id }).length).to.equal(1)
        })

        it('should successfully delete a todo', async () => {
            let todo = new Todo('test')
            Store.commit(TodoMutations.add, todo)
            expect(Store.state[TodoStoreName].todos.filter((t) => { return t.id === todo.id }).length).to.equal(1)
            Store.commit(TodoMutations.delete, todo)
            expect(Store.state[TodoStoreName].todos.filter((t) => { return t.id === todo.id }).length).to.equal(0)
        })
    })

    describe('actions', async () => {
        it('should successfully persist a todo', async () => {
            let todo = new Todo('test')
            await Store.dispatch(TodoActions.add, todo)
            let d = await db
            let t = await d.transaction(TodoStoreName).objectStore(TodoStoreName).get(todo.id)
            expect(Todo.fromJSON(t).id).to.equal(todo.id)
        })

        it('should successfully delete a todo', async () => {
            let todo = new Todo('test')
            await Store.dispatch(TodoActions.add, todo)
            await Store.dispatch(TodoActions.delete, todo)
            let d = await db
            let t = await d.transaction(TodoStoreName).objectStore(TodoStoreName).get(todo.id)
            expect(typeof t).to.equal('undefined')
        })

        it('should successfully retrieve the DB state into the store state', async () => {
            let todo = new Todo('test')
            let d = await db
            let tx = d.transaction(TodoStoreName, 'readwrite')
            tx.objectStore(TodoStoreName).put(todo.toJSON())
            await tx.complete
            await Store.dispatch(TodoActions.getDBState)
            expect(Store.state[TodoStoreName].todos.filter((t) => { return t.id === todo.id }).length).to.equal(1)
        })
    })
})
